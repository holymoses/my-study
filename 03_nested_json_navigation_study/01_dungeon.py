# -*- coding: utf-8 -*-

# rpg.json - карта, представляющая из себя json файл
# в ней есть строки - они же мобы, и локации - словари
# переход в локацию тратит время, убиение моба - тратит время и даёт очки опыта
# "Mob_exp20_tm200" + 20 единиц опыта и - 200 секунд времени
# задача - написать скрипт, который позволит игроку набрать 280 единиц опыта и успешно до локации с люком(hatch)
# до того как истечёт время
# взаимодействие с игроком происходит в следующем виде -
# Вы находитесь в location_name
# Вы можете убить следующих мобов - (___)
# Вы можете перейти в следующие локации - (___)
#


import re
import json
import csv
from decimal import Decimal
from datetime import datetime
from collections import defaultdict

field_names = ['current_location', 'current_experience', 'time left', 'current_date']
remaining_time = '123456.0987654321'


def input_tester(line):
    # Проверка правильности ввода
    slicer = [element for element in line.split('_')]
    actions = ('mob', 'loc')
    if len(slicer) == 2:
        if slicer[0] in actions and slicer[1].isdigit():
            return slicer
    return False


class Game:

    def __init__(self):
        with open('rpg.json', 'r') as rpg_json:
            self.current_location = json.load(rpg_json)
            self.current_location_name = ''
        self.csv_writer(fieldnames=field_names)
        self.time_spent = Decimal('0.00')
        self.time_left = Decimal('0.00')
        self.total_time = Decimal(remaining_time)
        self.exp = 0
        self.game_over = False
        self.dead_mobs = defaultdict(list)
        self.available_mobs = []
        self.available_ways = []
        self.current_time = datetime.now().strftime("%H:%M:%S")

    def csv_writer(self, fieldnames):
        with open('rpg.csv', 'a', newline='') as out_csv:
            writer = csv.writer(out_csv)
            writer.writerow(fieldnames)

    def get_options(self):
        # вычисляет оставшееся время, обновляет временные списки, нумеруя каждый найденный объект
        # если строка - моб
        # если словарь - локация
        self.available_ways.clear()
        self.available_mobs.clear()
        self.time_left = self.total_time - self.time_spent

        for key, value in self.current_location.items():
            self.current_location_name = key
            for num, content in enumerate(value):
                if isinstance(content, str) and [num, content] not in self.dead_mobs[self.current_location_name]:
                    self.available_mobs.append([num, content])
                elif isinstance(content, dict):
                    for location_key in content.keys():
                        self.available_ways.append([num, location_key])

        self.csv_writer(fieldnames=[self.current_location_name, self.exp, self.time_left, self.current_time])

    def test_conditions(self):
        # проверка на условия победы или поражения
        if self.time_left <= Decimal('0.00'):
            print('NOT WINRAR')
            self.game_over = True
        if 'Hatch' in self.current_location_name:
            if self.exp >= 280:
                print('WINRAR')
            else:
                print('NOT WINRAR')
            self.game_over = True
        elif not self.available_ways and not self.available_mobs:
            print('Dunno if that is about to happen, but here you go. Dead end.')
            self.game_over = True

    def number_searcher(self, action, number):
        # поиск в списках доступных действий номера выбранной локации или моба
        if action == 'loc':
            for data in self.available_ways:
                if number in data:
                    self.go_to(data)
                    return True
        elif action == 'mob':
            for data in self.available_mobs:
                if number in data:
                    self.kill_mob(data)
                    return True
        else:
            return False

    def input_handler(self):
        # вывод на экран информации, проверка ввода пользователя
        while True:

            print('Type "help" to see controls')
            print(f'You are in {self.current_location_name}, time left - {self.time_left}, exp - {self.exp}')
            print(f'You can kill {self.available_mobs}')
            print(f'You can go to {self.available_ways}')

            act = input('Your decision>>>')
            input_tests = input_tester(act)
            if act == 'help':
                print('_______________help_______________')
                print('Correct input - X_Y, where X - action("mob" to kill a mob or "loc" to go to location)')
                print('Y - number of a mob or a location, so the correct input will be:')
                print('loc_1, mob_0')
                print('If you wish to stop the game type "q"')
                print('If you wish to skip turn type "s"')
                print('_______________help_______________')
                continue
            elif act == 'q':
                print('You decide to quit the game')
                self.game_over = True
                break
            elif act == 's':
                print('You decide to skip turn')
                break
            elif input_tests:
                action, number = input_tests[0], input_tests[1]
                if not self.number_searcher(action=action, number=int(number)):
                    print('Number out of range, try again.')
                    continue
                else:
                    break
            else:
                print('Invalid input, try again.')

    def kill_mob(self, mob_data):
        # получение опыта из строки моба, вычет времени, затраченного на его убиение,
        # внесение моба в словарь убитых мобов, чтобы одного и того же нельзя было убить несколько раз
        mob_number = mob_data[0]
        mob_name = mob_data[1]
        exp_mob = re.search(r'exp\d+', mob_name).group()
        time_mob = re.search(r'tm\d+.\d+|tm\d+', mob_name).group()
        self.exp += int(exp_mob.replace('exp', ''))
        self.time_spent += Decimal(time_mob.replace('tm', ''))
        self.dead_mobs[self.current_location_name].append([mob_number, mob_name])

    def go_to(self, loc_data):
        # вычет времени затраченного на переход в локацию, обновление пути по файлу
        loc_number = loc_data[0]
        loc_name = loc_data[1]
        time_loc = re.search(r'tm\d+.\d+|tm\d+', loc_name).group()
        self.time_spent += Decimal(time_loc.replace('tm', ''))
        self.current_location = self.current_location[self.current_location_name][loc_number]

    def start(self):
        while True:
            self.get_options()
            self.test_conditions()
            if self.game_over:
                break
            self.input_handler()


if __name__ == '__main__':
    g = Game()
    g.start()

