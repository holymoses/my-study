import bowling
import unittest


class TestBowling(unittest.TestCase):

    def test_old_rules(self):
        s_c = bowling.ScoreCounter(score_line='Х4/34-4')
        s_c.run()
        res = s_c.total_score
        self.assertEqual(res, 46)

    def test_new_rules(self):
        s_c = bowling.ScoreCounter(score_line='ХXX347/21', new_rules=True)
        s_c.run()
        res = s_c.total_score
        self.assertEqual(res, 92)

    def test_frame_amount_old(self):
        s_c = bowling.ScoreCounter(score_line='ХXX347/21')
        s_c.frame_manager()
        res = len(s_c.frame_array)
        self.assertEqual(res, 6)

    def test_frame_amount_new(self):
        s_c = bowling.ScoreCounter(score_line='Х1/23459-X', new_rules=True)
        s_c.frame_manager()
        res = len(s_c.frame_array)
        self.assertEqual(res, 6)

    def test_invalid_symbol(self):
        with self.assertRaises(bowling.InvalidSymbol):
            s_c = bowling.ScoreCounter(score_line='ХXXa')
            s_c.run()

    def test_score_frame_error(self):
        with self.assertRaises(bowling.ScoreFrameError):
            s_c = bowling.ScoreCounter(score_line='99')
            s_c.run()

    def test_too_many_frames(self):
        with self.assertRaises(bowling.TooManyFrames):
            s_c = bowling.ScoreCounter(score_line='XXXXXXXXXXX')
            s_c.run()


if __name__ == '__main__':
    unittest.main()
