# -*- coding: utf-8 -*-

# Парсер протокола турнира по файлу tournament.exe
#
# Пример записи из лога турнира
#   ### Tour 1
#   Алексей	35612/----2/8-6/3/4/
#   Татьяна	62334/6/4/44X361/X
#   Давид	--8/--8/4/8/-224----
#   Павел	----15623113-95/7/26
#   Роман	7/428/--4-533/34811/
#   winner is .........
#
# Нужно сформировать выходной файл tournament_result.txt c записями вида
#   ### Tour 1
#   Алексей	35612/----2/8-6/3/4/    98
#   Татьяна	62334/6/4/44X361/X      131
#   Давид	--8/--8/4/8/-224----    68
#   Павел	----15623113-95/7/26    69
#   Роман	7/428/--4-533/34811/    94
#   winner is Татьяна


import argparse
from tournament_parser import TournamentParser

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to pars')
    parser.add_argument('rules', type=bool, help='apply new rules for parsing(old rules if False)')
    parser.add_argument('--output_file', type=str, help='Resulting file')
    args = parser.parse_args()
    A = TournamentParser(input_file=args.input_file, rules=args.rules, output_file=args.output_file)

