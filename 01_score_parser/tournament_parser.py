import bowling
from collections import defaultdict
from operator import itemgetter
import re

f = 'tournament.txt'


class TournamentParser:

    """
    Class for parsing bowling tournament results

    ### Tour 1
    #   Алексей	35612/----2/8-6/3/4/
    #   Татьяна	62334/6/4/44X361/X
    #   Давид	--8/--8/4/8/-224----
    #   Павел	----15623113-95/7/26
    #   Роман	7/428/--4-533/34811/
    #   winner is .........
    """

    def __init__(self, input_file, rules, output_file='tournament_with_results.txt'):
        self.input_file = input_file
        self.output_file = output_file
        self.rules = rules
        self.winner_choose = defaultdict(int)

    def has_cyrillic(self, text):
        return bool(re.search('[а-яА-Я]', text))

    def run(self):
        with open(self.input_file, 'r', encoding='utf8') as in_f, open(self.output_file, 'a', encoding='utf8') as out_f:
            for line in in_f:
                self.pars(line=line, file=out_f)

    def pars(self, line, file):
        if self.has_cyrillic(text=line):
            self.score_line(line=line, file=file)
        elif 'winner' in line:
            self.win_line(line=line, file=file)
        else:
            file.write(line)

    def win_line(self, line, file):
        winner = max(self.winner_choose.items(), key=itemgetter(1))[0]
        x, y, z = line.split()
        new_line = f'{x} {y} {winner}\n'
        file.write(new_line)
        self.winner_choose.clear()

    def score_line(self, line, file):
        name, score = line.split('\t')
        score = score.replace('\n', '')
        try:
            score_counter = bowling.ScoreCounter(score_line=score, new_rules=self.rules)
            score_counter.run()
            res = score_counter.total_score
            self.winner_choose[name] = res
            new_line = f'{name} {score} {res}\n'
            file.write(new_line)
        except:
            self.exc_found(name=name, file=file)

    def exc_found(self, name, file):
        self.winner_choose[name] = 0
        new_line = f'{name} -некорректные данные- 0\n'
        file.write(new_line)


A = TournamentParser(input_file=f, rules=True)
A.run()


