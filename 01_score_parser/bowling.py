import re


class InvalidSymbol(Exception):
    pass


class TooManyFrames(Exception):
    pass


class ScoreFrameError(Exception):
    pass


class ScoreCounter:
    xs = 'xXхХ'

    def __init__(self, score_line, new_rules=False):
        self.score_line = re.sub(r'[-]', '0', score_line)  # replaces every '-' with '0' for convenience
        self.new_rules = new_rules
        self.total_score = 0
        self.prev = None
        self.frame_array = []

    def frame_manager(self):
        """
        :return: list object with every frame(and additional points if new_rules = True) in it
        """
        i = 0
        while i <= len(self.score_line) - 1:
            if self.score_line[i] in ScoreCounter.xs:
                if self.new_rules:
                    self.frame_array.append(self.score_line[i:i+3])
                else:
                    self.frame_array.append((self.score_line[i]))
                i += 1
            elif '0' <= self.score_line[i] <= '9':
                if self.new_rules and self.score_line[i+1] == '/':
                    self.frame_array.append(self.score_line[i:i+3])
                else:
                    self.frame_array.append(self.score_line[i:i+2])
                i += 2
            else:
                raise InvalidSymbol()
        if len(self.frame_array) > 10:
            raise TooManyFrames

    def first_throw(self, symbol):
        if symbol in ScoreCounter.xs:
            self.total_score += 10 if self.new_rules else 20
        elif '0' <= symbol <= '9':
            self.prev = symbol
            self.total_score += int(symbol)
        else:
            raise InvalidSymbol()

    def second_throw(self, symbol):
        if symbol == '/':
            self.total_score -= int(self.prev)
            self.total_score += 10 if self.new_rules else 15
        elif '0' <= symbol <= '9':
            if int(self.prev) + int(symbol) > 10:
                raise ScoreFrameError()
            elif int(self.prev) + int(symbol) <= 10:
                self.total_score += int(symbol)
        else:
            raise InvalidSymbol()

    def run(self):
        """
        Counts score for every frame in frame list, created by method frame_manager
        """
        self.frame_manager()
        for elem in self.frame_array:
            self.prev = None
            for symbol in elem:
                if self.prev is None:
                    self.first_throw(symbol)
                else:
                    self.second_throw(symbol)
                    self.prev = None
