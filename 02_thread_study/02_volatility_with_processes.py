# -*- coding: utf-8 -*-


# Задача: вычислить 3 тикера с максимальной и 3 тикера с минимальной волатильностью в МНОГОПРОЦЕССНОМ стиле
#
# Бумаги с нулевой волатильностью вывести отдельно.
# Результаты вывести на консоль в виде:
#   Максимальная волатильность:
#       ТИКЕР1 - ХХХ.ХХ %
#       ТИКЕР2 - ХХХ.ХХ %
#       ТИКЕР3 - ХХХ.ХХ %
#   Минимальная волатильность:
#       ТИКЕР4 - ХХХ.ХХ %
#       ТИКЕР5 - ХХХ.ХХ %
#       ТИКЕР6 - ХХХ.ХХ %
#   Нулевая волатильность:
#       ТИКЕР7, ТИКЕР8, ТИКЕР9, ТИКЕР10, ТИКЕР11, ТИКЕР12
#

import os
from operator import itemgetter
from multiprocessing import Process, Queue
from collections import defaultdict
from queue import Empty


folder = r'..\..\..\trades'


class VolatilityGetter(Process):

    def __init__(self, filename, vol_cont, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filename = filename
        self.tick = None
        self.volatility = 0
        self.min = None
        self.max = 0
        self.vol_cont = vol_cont

    def formula(self, min_price, max_price):
        half = (min_price + max_price) / 2
        return ((max_price - min_price) / half) * 100

    def run(self):
        with open(self.filename, 'r') as file_to_read:
            for line in file_to_read:
                self.tick, time, price, quant = line.split(',')
                if self.tick == 'SECID':
                    continue
                else:
                    if self.vol_cont.full():
                        pass
                    price = float(price)
                    if price > self.max:
                        self.max = price
                    if self.min is None or price < self.min:
                        self.min = price
            self.volatility = round(self.formula(min_price=self.min, max_price=self.max), ndigits=2)
            res = str(self.tick) + str(self.volatility)
            self.vol_cont.put(res)


class CounterContainer(Process):

    def __init__(self, vol_dict, files, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.files = files
        self.counters = []
        self.vol_dict = vol_dict
        self.vol_que = Queue(maxsize=1)
        self.to_ret = Queue(maxsize=1)

    def run(self):
        for x in self.files:
            counter = VolatilityGetter(filename=x, vol_cont=self.vol_que)
            self.counters.append(counter)
        for counter in self.counters:
            counter.start()
        while True:
            try:
                res = self.vol_que.get(timeout=1)
                tick = res[0:4]
                vol = float(res[4:])
                self.vol_dict[tick] = vol
            except Empty:
                if not any(counter.is_alive() for counter in self.counters):
                    break
        self.to_ret.put(self.vol_dict)
        for counter in self.counters:
            counter.join()


paths = []
volatility_dict = defaultdict(int)
volatility_selected = {}

for dirname, dirpath, filenames in os.walk(folder):
    for file in filenames:
        path = os.path.join(folder, file)
        paths.append(path)

if __name__ == '__main__':
    container = CounterContainer(vol_dict=volatility_dict, files=paths)
    container.start()
    container.join()
    volatility_dict = container.to_ret.get(timeout=1)

    for i, e in sorted(volatility_dict.items(), key=itemgetter(1), reverse=True):
        if len(volatility_selected) == 3:
            break
        volatility_selected[i] = e

    for i, e in sorted(volatility_dict.items(), key=itemgetter(1), reverse=False):
        if len(volatility_selected) == 6:
            break
        if e == 0.0:
            continue
        else:
            volatility_selected[i] = e

    for i, e in sorted(volatility_dict.items(), key=itemgetter(1), reverse=False):
        if e > 0:
            continue
        else:
            volatility_selected[i] = e

count = 0
for i, e in sorted(volatility_selected.items(), key=itemgetter(1), reverse=True):
    count += 1
    if count == 1:
        print('\nМаксимальная волатильность:')
    elif count == 4:
        print('\nМинимальная волатильность:')
    elif count == 7:
        print('\nНулевая волатильность:')
    print(f'{i}:{e}')

